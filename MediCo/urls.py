from django.conf.urls import patterns, include, url
from django.contrib import admin

urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'MediCo.views.home', name='home'),
    # url(r'^blog/', include('blog.urls')),

    url(r'^RapidReferral/', include('RapidReferral.urls', namespace='RapidReferral')),
    url(r'^admin/', include(admin.site.urls)),
)
