from django.db import models
from django import forms
from django.core.urlresolvers import reverse

class MyModelMixin(object):
    
    def _get_field_names(self):
        field_names = [field.name for field in self._meta.fields]
        return field_names
    field_names = property(_get_field_names)
    
    def _get_all_field_data(self):
        fields_data = [getattr(self, field) for field in self.field_names]
        return fields_data
    all_field_data = property(_get_all_field_data)
    

class Address(MyModelMixin, models.Model):
    street = models.CharField(max_length=50, blank=True)
    city = models.CharField(max_length=50)
    state = models.CharField(max_length=2)
    
    def __unicode__(self):
        return self.street + ' ' + self.city + ', ' + self.state
    

class Person(MyModelMixin, models.Model):
    first_name = models.CharField(max_length=50)
    last_name = models.CharField(max_length=50)
    home_address = models.ForeignKey(Address, blank=True, null=True)
    
    class Meta:
        ordering = ['last_name']
    
    def _get_full_name(self):
        return self.first_name + ' ' + self.last_name
    full_name = property(_get_full_name)
    
    def __unicode__(self):
        return self.last_name + ', ' + self.first_name

class Patient(Person):
    pcp = models.ForeignKey('Doctor', blank=True, null=True)
    
    def get_absolute_url(self):
        return reverse('RapidReferral:patient_detail', kwargs={'patient_id': self.id})
    
class Doctor(Person):
    patients = models.ManyToManyField(Patient, blank=True, null=True)
    specialties = models.ManyToManyField('Specialty')
    #maybe add house_calls bool. if so move home_address to person model
    
    def get_absolute_url(self):
        return reverse('RapidReferral:doctor_detail', kwargs={'doctor_id': self.id})
    
class Office(MyModelMixin, models.Model):
    name = models.CharField(max_length=50, blank=True)
    address = models.ForeignKey(Address)
    doctors = models.ManyToManyField(Doctor)
    
    class Meta:
        ordering = ['name']
    
    def get_absolute_url(self):
        return reverse('RapidReferral:office_detail', kwargs={'office_id': self.id})
    
    def __unicode__(self):
        return self.name
    
    
    
class Specialty(MyModelMixin, models.Model):
    specialty = models.CharField(max_length=50, primary_key=True)
    
    class Meta:
        ordering = ['specialty']
    
    def __unicode__(self):
        return self.specialty
    
class Appointment(MyModelMixin, models.Model):
    patient = models.ForeignKey(Patient)
    specialty = models.ForeignKey(Specialty)
    office = models.ForeignKey(Office)
    doctor = models.ForeignKey(Doctor)
    time = models.DateTimeField()
    
    def get_absolute_url(self):
        return reverse('RapidReferral:appointment_detail', kwargs={'appointment_id' : self.id})
                
class OfficeFilterForm(forms.Form):
    specialty = forms.ModelChoiceField(queryset=Specialty.objects.all(), required=False)
    doctor = forms.ModelChoiceField(queryset=Doctor.objects.all(), required=False)
    #address: to be added to filter by address and proximity
    #proximity: to be added.
    
   
    
                
class PatientForm(forms.ModelForm):
    class Meta:
        model = Patient
        fields = '__all__'
        
class DoctorForm(forms.ModelForm):
    class Meta:
        model = Doctor
        exclude = ['home_address'] 
 
class AppointmentForm(forms.ModelForm):
    class Meta:
        model = Appointment
        fields = '__all__' #['patient', 'doctor', 'specialty', 'office', 'time']
        
class OfficeForm(forms.ModelForm):
    class Meta:
        model = Office
        fields = '__all__'
        
# class AppointmentFilterForm(AppointmentForm):
    # def __init__(self, *args, **kwargs):
        # super(AppointmentFilterForm, self).__init__(*args, **kwargs)
        # self.fields['time2'] = forms.fields.DateTimeField()
        # del self.fields['patient']
        
        # #for k, v in self.fields.items():
            # v.required = False
               
    # class Meta(AppointmentForm.Meta):
        # pass
        
class AppointmentFilterForm(forms.Form):       
    patient = forms.ModelChoiceField(queryset=Patient.objects.all(), required=False)
    doctor = forms.ModelChoiceField(queryset=Doctor.objects.all(), required=False)
    specialty = forms.ModelChoiceField(queryset=Specialty.objects.all(), required=False)
    office = forms.ModelChoiceField(queryset=Office.objects.all(), required=False)
    time = forms.DateTimeField(required=False)
    time2 = forms.DateTimeField(required=False)
        
        
        
        
        
        
        
        