# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('RapidReferral', '0006_appointment'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='office',
            options={'ordering': ['name']},
        ),
        migrations.AlterModelOptions(
            name='person',
            options={'ordering': ['last_name']},
        ),
        migrations.AlterModelOptions(
            name='specialty',
            options={'ordering': ['specialty']},
        ),
    ]
