# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('RapidReferral', '0005_auto_20141206_1156'),
    ]

    operations = [
        migrations.CreateModel(
            name='Appointment',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('time', models.DateTimeField()),
                ('doctor', models.ForeignKey(to='RapidReferral.Doctor')),
                ('office', models.ForeignKey(to='RapidReferral.Office')),
                ('patient', models.ForeignKey(to='RapidReferral.Patient')),
                ('specialty', models.ForeignKey(to='RapidReferral.Specialty')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
    ]
