# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('RapidReferral', '0003_auto_20141206_1145'),
    ]

    operations = [
        migrations.AddField(
            model_name='office',
            name='name',
            field=models.CharField(default=b'', max_length=50, blank=True),
            preserve_default=True,
        ),
    ]
