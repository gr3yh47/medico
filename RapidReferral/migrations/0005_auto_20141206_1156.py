# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('RapidReferral', '0004_office_name'),
    ]

    operations = [
        migrations.AlterField(
            model_name='office',
            name='name',
            field=models.CharField(max_length=50, blank=True),
            preserve_default=True,
        ),
    ]
