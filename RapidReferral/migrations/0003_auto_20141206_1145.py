# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('RapidReferral', '0002_remove_address_number'),
    ]

    operations = [
        migrations.AlterField(
            model_name='doctor',
            name='patients',
            field=models.ManyToManyField(to='RapidReferral.Patient', null=True, blank=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='patient',
            name='pcp',
            field=models.ForeignKey(blank=True, to='RapidReferral.Doctor', null=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='person',
            name='home_address',
            field=models.ForeignKey(blank=True, to='RapidReferral.Address', null=True),
            preserve_default=True,
        ),
    ]
