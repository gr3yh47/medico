# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Address',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('number', models.CharField(max_length=20, blank=True)),
                ('street', models.CharField(max_length=50, blank=True)),
                ('city', models.CharField(max_length=50)),
                ('state', models.CharField(max_length=2)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Office',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('address', models.ForeignKey(to='RapidReferral.Address')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Person',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('first_name', models.CharField(max_length=50)),
                ('last_name', models.CharField(max_length=50)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Patient',
            fields=[
                ('person_ptr', models.OneToOneField(parent_link=True, auto_created=True, primary_key=True, serialize=False, to='RapidReferral.Person')),
            ],
            options={
            },
            bases=('RapidReferral.person',),
        ),
        migrations.CreateModel(
            name='Doctor',
            fields=[
                ('person_ptr', models.OneToOneField(parent_link=True, auto_created=True, primary_key=True, serialize=False, to='RapidReferral.Person')),
            ],
            options={
            },
            bases=('RapidReferral.person',),
        ),
        migrations.CreateModel(
            name='Specialty',
            fields=[
                ('specialty', models.CharField(max_length=50, serialize=False, primary_key=True)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.AddField(
            model_name='person',
            name='home_address',
            field=models.ForeignKey(to='RapidReferral.Address', null=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='patient',
            name='pcp',
            field=models.ForeignKey(to='RapidReferral.Doctor', null=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='office',
            name='doctors',
            field=models.ManyToManyField(to='RapidReferral.Doctor'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='doctor',
            name='patients',
            field=models.ManyToManyField(to='RapidReferral.Patient', null=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='doctor',
            name='specialties',
            field=models.ManyToManyField(to='RapidReferral.Specialty'),
            preserve_default=True,
        ),
    ]
