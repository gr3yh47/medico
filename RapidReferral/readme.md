## Synopsis

This project is a personal exercise - a simple data driven web app for tracking doctor/patient/clinic relationships. Sorting and filtering of data is possible thanks to some light API-style JSON payloads that are passed between filter forms on the view and the controller logic. 

## Installation

install python 2.7 and django 1.7+

python manage.py runserver to start up the built-in dev server

navigate to localhost:8000/RapidReferral/doctors/ and use the menu controls at the top for navigation

## API Reference

This is not a true api as it is written in classic django framework style, however there is an API-esque endpoint that can be accessed - specifically /RapidReferral/offices/JSON

I intend to transform this app to use more API style endpoints in the future

## License

Public domain, do as you like with it. 
