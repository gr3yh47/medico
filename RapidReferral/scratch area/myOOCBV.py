APP_URL = 'RapidReferral/'

class BaseView(View):
    model = None
    specific_context = ''
    order_key = ''
    templates = {}
    
    def list(self, request):
        object_list = model.objects.order_by(order_key)
        context = {'object_list': object_list}
        return render(request, APP_URL + self.templates.list, context)
        
    def detail(self, request, object_id):
        object = get_object_or_404(self.model, object_id)
        context = {specific_context: object}
        return render(request, APP_URL + self.templates.detail)
        

class DoctorView(BaseView):
    model = Doctor
    specific_context = 'doctor'
    order_key = 'last_name'
    templates = {'list': 'doctor_list.html',
                 'detail': 'doctor_detail.html',
                 'entry': 'base_form.html'}


