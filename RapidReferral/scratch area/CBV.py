APP_TEMPLATE_DIR = 'RapidReferral/'

class BaseMixin(object):
    templates = {'list': 'base_list.html',
                      'detail': 'base_detail.html',
                      'form': 'base_form.html',
                      'submission': 'base_submission.html'}
   
    def get_template(type):
        return APP_TEMPLATE_DIR = templates[type]

class DoctorMixin(BaseMixin):
    model = Doctor
    templates[list] = 'doctor_list.html'
    templates[detail] = 'doctor_detail.html'
    
class DoctorList(DoctorMixin, ListView):
    context_object_name = 'object_list'
    template_name = get_template('list')
    
    
class DoctorDetail(DoctorMixin, DetailView):
    pass
    
    