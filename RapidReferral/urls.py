from django.conf.urls import include, patterns, url
from RapidReferral import views


doctors_patterns = patterns('',

    # /RapidReferral/doctors/
    url(r'^$', views.doctors, name='doctors'),
    
    # /RapidReferral/doctors/1/
    url(r'^(?P<doctor_id>\d+)/$', views.doctor_detail, name='doctor_detail'),
    
    #/RapidReferral/doctors/create/
    url(r'^create/$', views.doctor_entry, name='doctor_entry'),

)    

patients_patterns = patterns('',
    
    # /RapidReferral/patients/
    url(r'^$', views.patients, name='patients'),
    
    # /RapidReferral/patients/1/
    url(r'^(?P<patient_id>\d+)/$', views.patient_detail, name='patient_detail'),
    
    #/RapidReferral/patients/create/
    url(r'^create/$', views.patient_entry, name='patient_entry'),
    
)

offices_patterns = patterns('',
    
    # /RapidReferral/offices/
    url(r'^$', views.offices, name='offices'),
    
    # /RapidReferral/offices/1/
    url(r'^(?P<office_id>\d+)/$', views.office_detail, name='office_detail'),
    
    #/RapidReferral/offices/create/
    url(r'^create/$', views.office_entry, name='office_entry'),
    
    # /RapidReferral/offices/JSON
    url(r'^(?P<format>\w+)/$', views.offices, name='offices_formatted')
    
)

appointments_patterns = patterns('',
    
    # /RapidReferral/appointments/
    url(r'^$', views.appointments, name='appointments'),
    
    # /RapidRederral/appointments/1/
    url(r'^(?P<appointment_id>\d+)/$', views.appointment_detail, name='appointment_detail'),
    
    #/RapidReferral/appointment/create/
    url(r'^create/$', views.appointment_entry, name='appointment_entry'),
    
)


urlpatterns = patterns('',

    # /RapidReferral/
    url(r'^$', views.index, name='index'),
    
    # /RapidReferral/doctors/...
    url(r'^doctors/', include(doctors_patterns)), 
    
    # /RapidReferral/patients/...
    url(r'^patients/', include(patients_patterns)), 
    
    # /RapidReferral/offices/...
    url(r'^offices/', include(offices_patterns)), 
    
    # /RapidReferral/appointments/...
    url(r'^appointments/', include(appointments_patterns)), 
    
)


    