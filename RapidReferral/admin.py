from django.contrib import admin

from RapidReferral.models import Patient, Doctor, Office, Specialty, Address, Person

#class AddressInline(admin.StackedInline):
    #model = Patient

class PatientAdmin(admin.ModelAdmin):
    #inlines = [AddressInline]
    fields = ['first_name', 'last_name', 'home_address', 'pcp']
    
#class DoctorAdmin(admin.ModelAdmin):
    #ields = ['specialty']
    


    
admin.site.register(Patient, PatientAdmin)
admin.site.register(Doctor)#, DoctorAdmin)
admin.site.register(Office)
admin.site.register(Specialty)
admin.site.register(Address)