from django.shortcuts import get_object_or_404, redirect, render
from django.http import HttpResponseRedirect, HttpResponse
from django.template import RequestContext, loader
from django.views.generic import View
from django.core.urlresolvers import reverse
from django.core import serializers
from RapidReferral.models import Doctor, DoctorForm, Patient, PatientForm, Office, OfficeForm, OfficeFilterForm, Appointment, AppointmentForm, AppointmentFilterForm
import operator
import datetime
import pdb

def index(request):
    #needs a real index page
    return HttpResponse("RR index page")
 
def doctors(request, format = 'HTML'):
    d_list = Doctor.objects.all()
    if request.GET:
        form = DoctorFilterForm(request.GET)
        sp_filter = form.cleaned_data['specialty']
        o_filter = form.cleaned_data['office']
        if form.is_valid:
            if sp_filter:
                d_list = d_list.filter(specialties__specialty=sp_filter)
            
            
        
        
    doctor_list = Doctor.objects.order_by('last_name')
    context = {'object_list': doctor_list}
    return render(request, 'RapidReferral/doctor_list.html', context)
 
def doctor_detail(request, doctor_id):
    doctor = get_object_or_404(Doctor, pk=doctor_id)
    context = {'doctor': doctor}
    return render(request, 'RapidReferral/doctor_detail.html', context)
    
def doctor_entry(request):
    doctor_form = DoctorForm()
    context = {'form': doctor_form}
    return render(request, 'RapidReferral/base_form.html', context)
    
def patients(request):
    patient_list = Patient.objects.order_by('last_name')
    context = {'object_list': patient_list}
    return render(request, 'RapidReferral/patient_list.html', context)

def patient_detail(request, patient_id):
    patient = get_object_or_404(Patient, pk=patient_id)
    context = {'patient': patient}
    return render(request, 'RapidReferral/patient_detail.html', context)
    
def patient_entry(request):
    patient_form = PatientForm()
    context = {'form': patient_form}
    return render(request, 'RapidReferral/base_form.html', context)

def offices(request, format = 'HTML'):
    o_list = Office.objects.all()
    if request.GET:
        form = OfficeFilterForm(request.GET)
        if form.is_valid():
            sp_filter = form.cleaned_data['specialty']            
            d_filter = form.cleaned_data['doctor']
            if form.cleaned_data['specialty']:
                o_list = o_list.filter(doctors__specialties__specialty=sp_filter)
            if form.cleaned_data['doctor']:
                o_list = o_list.filter(doctors=d_filter)
    else:
        form = OfficeFilterForm()
        
    context = {'object_list': o_list, 'form': form}
    if format == 'HTML':
        return render(request, 'RapidReferral/base_list_table_filtered.html', context)
    elif format == 'JSON':
        return HttpResponse(serializers.serialize('json', o_list))

def office_detail(request, office_id):
    office = get_object_or_404(Office, pk=office_id)
    context = {'office': office}
    return render(request, 'RapidReferral/office_detail.html', context)
    
def office_entry(request):
    
    if request.method == 'POST':
        form = OfficeForm(request.POST)
        if form.is_valid():     
            new_office = form.save()
            return redirect(new_office)                      
    else:
        form = OfficeForm()
    context = {'form': form}
    return render(request, 'RapidReferral/base_form.html', context)

def specialties(request):
    pass
    
def specialty_detail(request, specialty_id):
    pass
    
def appointments(request):
    qs = Appointment.objects.all()
    if request.GET:
        form = AppointmentFilterForm(request.GET)
        if form.is_valid():
        
            start_date_time, end_date_time = form.cleaned_data['time'], form.cleaned_data['time2']
            if start_date_time:
                qs = qs.filter(time__gt=start_date_time)
            if end_date_time:
                qs = qs.filter(time__lt=end_date_time)
               
            for field_name, field_value in form.cleaned_data.iteritems():
                if field_value and 'time' not in field_name:
                    qs = qs.filter(**{field_name:field_value})
    else:
        form = AppointmentFilterForm()
    appointment_list = qs.order_by('time')
    context = {'object_list': appointment_list, 'form': form}
    return render(request, 'RapidReferral/appointment_list_table_filtered.html', context)
    
def appointment_detail(request, appointment_id, appointment_context=None):
    appointment = get_object_or_404(Appointment, pk=appointment_id)
    context = {'appointment': appointment}
    return render(request, 'RapidReferral/appointment_detail.html', context)
    
def appointment_entry(request):
    if request.method == 'POST':
        form = AppointmentForm(request.POST)
        if form.is_valid():     
            new_appointment = form.save()
            
            d = new_appointment.doctor
            p = new_appointment.patient
            if not p.doctor_set.filter(id=d.id).count:
                d.patients.add(p)
                            
            return redirect(new_appointment)      
            #return redirect('appointment_detail', new_appointment.id)  
                
    else:
        form = AppointmentForm()
    context = {'form': form}
    return render(request, 'RapidReferral/appointment_form.html', context)

# def json_offices(request):
    # o_list = Office.
        
#def referral_search(request):
    
#def referral_result(request):